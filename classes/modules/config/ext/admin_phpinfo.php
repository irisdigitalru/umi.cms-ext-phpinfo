<?php
class admin_phpinfo extends def_module
{
  public function onInit() {
    $cmsController = cmsController::getInstance();

    $isAdminMode = false;
    if (method_exists($cmsController, 'isCurrentModeAdmin')) {
      $isAdminMode = $cmsController->isCurrentModeAdmin();
    } else {
      $isAdminMode = ($cmsController->getCurrentMode() == 'admin');
    }
    if ($isAdminMode) {

      $commonTabs = $this->getCommonTabs();

      if ($commonTabs instanceof iAdminModuleTabs) {
          $commonTabs->add('info');
      }
    }
  }

  public function info() {
    ob_start();
    phpinfo();
    $info = ob_get_contents();
    ob_end_clean();
    $info = preg_replace("/^.*?\<body\>/is", "", $info);
    $info = preg_replace("/<\/body\>.*?$/is", "", $info);

    $data['node:void'] = $info;

    $this->setDataType("list");
    $this->setActionType("view");

    $this->setData($data);
    $this->doData();
  }
}
