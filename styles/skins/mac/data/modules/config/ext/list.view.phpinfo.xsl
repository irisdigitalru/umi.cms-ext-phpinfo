<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet SYSTEM "ulang://common" [
]>


<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xlink="http://www.w3.org/TR/xlink">

  <xsl:template match="/result[@method = 'info']/data[@type = 'list' and @action = 'view']">
    <style type="text/css">
    .phpinfo-container {background-color: #fff; color: #222; font-family: sans-serif;}
    .phpinfo-container pre {margin: 0; font-family: monospace;}
    .phpinfo-container a {color: #222; text-decoration: none; background-color: #fff;}
    .phpinfo-container a:link {color: #009; text-decoration: none; background-color: #fff;}
    .phpinfo-container a:hover {text-decoration: underline;}
    .phpinfo-container table {border-collapse: collapse; border: 0; width: 934px; box-shadow: 1px 2px 3px #ccc;}
    .phpinfo-container .center {text-align: center;}
    .phpinfo-container .center table {margin: 1em auto; text-align: left;}
    .phpinfo-container .center th {text-align: center !important;}
    .phpinfo-container td, .phpinfo-container th {border: 1px solid #666; vertical-align: baseline; padding: 4px 5px;}
    .phpinfo-container h1 {font-size: 150%; text-transform: none;}
    .phpinfo-container h2 {font-size: 125%; text-transform: none;}
    .phpinfo-container .p {text-align: left;}
    .phpinfo-container .e {background-color: #ccf; width: 300px; font-weight: bold;}
    .phpinfo-container .h {background-color: #99c; font-weight: bold;}
    .phpinfo-container .v {background-color: #ddd; max-width: 300px; overflow-x: auto;}
    .phpinfo-container .v i {color: #999;}
    .phpinfo-container img {float: right; border: 0;}
    .phpinfo-container hr {width: 934px; background-color: #ccc; border: 0; height: 1px;}
    </style>
    <div class="phpinfo-container">
      <xsl:value-of select="." disable-output-escaping="yes"/>
    </div>
  </xsl:template>

</xsl:stylesheet>
